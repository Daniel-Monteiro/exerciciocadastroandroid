package com.example.daniel.exercciocadastro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class ListaSorvete extends Activity{

    static ListView listaSorvetesADT;
    JDBCmodel jData = JDBCmodel.INSTANCE;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lista_sorvete);

        listaSorvetesADT = (ListView) findViewById(R.id.ListaSorvetes);

        List<Sorvete> listaSorvete = jData.getListaSorvete();

        ArrayAdapter<Sorvete> adapter = new ArrayAdapter<Sorvete>(this,
                android.R.layout.simple_list_item_1, listaSorvete);

        listaSorvetesADT.setAdapter(adapter);
    }

    public void btnVoltaMenu(View view) {
        Intent intent = new Intent(this,ActivityMain.class);
        startActivity(intent);
    }
}
