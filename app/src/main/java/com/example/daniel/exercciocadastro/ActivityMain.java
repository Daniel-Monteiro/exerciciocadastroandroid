package com.example.daniel.exercciocadastro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

public class ActivityMain extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void btnCadastro(View view) {
        Intent intent = new Intent(this,CadastroSorvete.class);
        startActivity(intent);
    }

    public void btnListaSorvete(View view) {
        Intent intent = new Intent(this,ListaSorvete.class);
        startActivity(intent);
    }
}
