package com.example.daniel.exercciocadastro;

import java.util.ArrayList;

public class Sorvete {

    private String nome;
    private String biscoito;
    private String saborPrincipal;
    private ArrayList<String>listaCobretura;
    private String text = "";

    public ArrayList<String> getListaCobretura() {
        return listaCobretura;
    }

    public void setListaCobretura(ArrayList<String> listaCobretura) {
        this.listaCobretura = listaCobretura;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getBiscoito() {
        return biscoito;
    }

    public void setBiscoito(String biscoito) {
        this.biscoito = biscoito;
    }

    public String getSaborPrincipal() {
        return saborPrincipal;
    }

    public void setSaborPrincipal(String saborPrincipal) {
        this.saborPrincipal = saborPrincipal;
    }

    public void addSabor(String sabor){
       this.listaCobretura.add(sabor);
    }

    @Override
    public String toString(){
        return "Nome: "+getNome()+", Sabor Principal: "+getSaborPrincipal()+", Biscoito: "+getBiscoito()+", Lista Cobertura: "+
               coberturaFormat();
    }

    public String coberturaFormat(){
        for(int i=0;i<listaCobretura.size();i++){
            this.text += listaCobretura.get(i).toString()+",";
        }
        return this.text;
    }
}
