package com.example.daniel.exercciocadastro;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;


public class CadastroResultado extends Activity {

    private ArrayList<String> listaCobertura = new ArrayList<>();

    TextView nomeSorvete;
    TextView saidaSabor;
    TextView saidaCobertura;
    TextView saidaBiscoito;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cadastro_resultado);

        nomeSorvete = (TextView) findViewById(R.id.saidaNomeSorvete);
        saidaSabor = (TextView) findViewById(R.id.saidaSabor);
        saidaCobertura = (TextView) findViewById(R.id.saidaCobertura);
        saidaBiscoito = (TextView) findViewById(R.id.saidaBiscoito);

        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        nomeSorvete.setText(bundle.getString("nomeSorvete"));

        saidaSabor.setText(bundle.getString("sabor"));

        listaCobertura = bundle.getStringArrayList("listaCobertura");
        String cobertura = new String();

        if(listaCobertura.isEmpty()) {
            cobertura = "Sem Cobertura";
        }else{
            for(int i = 0; i<listaCobertura.size();i++){
                cobertura += listaCobertura.get(i)+",";
            }
        }

        saidaCobertura.setText(cobertura);

        saidaBiscoito.setText(bundle.getString("biscoito"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        listaCobertura.clear();
    }

    public void btnVoltar(View view) {
        Intent intent = new Intent(this,ActivityMain.class);
        startActivity(intent);
    }

    public void btnVoltar2(View view) {
        Intent intent = new Intent(this,CadastroSorvete.class);
        startActivity(intent);
    }

    public void btnListarIce(View view){
        Intent intent = new Intent(this, ListaSorvete.class);
        startActivity(intent);
    }
}
