package com.example.daniel.exercciocadastro;

import java.util.ArrayList;

public enum JDBCmodel {

    INSTANCE;

    private ArrayList<Sorvete>ListaSorvete = new ArrayList<>();

    public ArrayList<Sorvete> getListaSorvete() {
        return ListaSorvete;
    }

    public void setListaSorvete(ArrayList<Sorvete> listaSorvete) {
        ListaSorvete = listaSorvete;
    }

    public void addSorvete(Sorvete sorvete){
        this.ListaSorvete.add(sorvete);
    }
}
