package com.example.daniel.exercciocadastro;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> listaCobretura = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton btSalvar = (ImageButton) findViewById(R.id.imageButtonSalvar);
        final EditText editNomeSorvete = (EditText) findViewById(R.id.editTextNomeSorvete);
        editNomeSorvete.requestFocus();

        btSalvar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            CheckBox checkBoxEsquimo = (CheckBox) findViewById(R.id.checkBoxEsquimó);
            CheckBox checkBoxCaramelo = (CheckBox) findViewById(R.id.checkBoxCaramelo);
            CheckBox checkBoxChocolate = (CheckBox) findViewById(R.id.checkBoxChocolate);
            CheckBox checkBoxMorango = (CheckBox) findViewById(R.id.checkBoxMorango);

            if (checkBoxEsquimo.isChecked()) {
                listaCobretura.add("Esquimó");
            }
            if (checkBoxCaramelo.isChecked()) {
                listaCobretura.add("Caramelo");
            }
            if (checkBoxChocolate.isChecked()) {
                listaCobretura.add("Chocolate");
            }
            if (checkBoxMorango.isChecked()) {
                listaCobretura.add("Morango");
            }

            Spinner spinner = (Spinner) findViewById(R.id.spinnerSabor);
            String Sabor = spinner.getSelectedItem().toString();

            RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
            int selectid = radioGroup.getCheckedRadioButtonId();

            RadioButton radioButtonBiscoito = (RadioButton) findViewById(selectid);
            String biscoito = radioButtonBiscoito.getText().toString();

            String nomeSorvete = editNomeSorvete.getText().toString();

            Intent intent = new Intent(v.getContext(), CadastroResultado.class);

            Bundle bundle =  new Bundle();

            bundle.putString("sabor",Sabor);
            bundle.putString("biscoito",biscoito);
            bundle.putStringArrayList("listaCobertura",listaCobretura);
            bundle.putString("nomeSorvete",nomeSorvete);
            intent.putExtras(bundle);

            Toast toast = Toast.makeText(v.getContext(),"Sorvete Criado!",Toast.LENGTH_LONG);
            toast.show();

            startActivity(intent);
        }
    });
    }
}
